use std::rc::Rc;

fn main() {
	let x: Rc<i32> = Rc::new(7);
	let y: Rc<i32> = x.clone();  // poveca se Rc brojac (ne stvara se kopija)
}


