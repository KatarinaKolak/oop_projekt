fn main(){
	let string = String::from("My String");  // stvara se objekt na heapu
	let str2 = print(string); // string se premjesta u funkciju print, povratna vrijednost premjesti vlasnistvo na str2
	println!("Returned value: {}",str2);    // ispis -> My.String
	println!("String value: {}", string ); // Greska!!!
	println!("Print function: {}", print(string)); // Greska!!!
}
fn print(string: String) -> String{
	let mut r_str = String::new();
	for c in string.chars(){
		if c != ' '{
			r_str.push(c);
		}
		else{
			r_str.push('.');
		}
	}
	println!("Value: {}",string);
	r_str  // vracanje stringa r_str
}



fn to_lower(string: String)-> String{
	let mut lower = string.to_lowercase(); // prebacuje velika slova u mala
	lower.push('!');  // dodaje na kraj stringa
	lower
}

fn main(){
	let string = String::from("STRING EXAMPLE");  
	println!("{}",to_lower(string));  // string example!
}

