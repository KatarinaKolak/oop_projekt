fn print_ECTS(c: &str) -> &str {
    match c {
		"OOP" => "6 ECTS",
        "PNI" => "6 ECTS",
        "DM" => "6 ECTS",
		"OWS" => "6 ECTS",
		"UUAF" => "4 ECTS",
		_ => "Nema unesenog predmeta",
    }
}

fn main(){
	println!("{}", print_ECTS("OWS"));  // 6 ECTS
	println!("{}", print_ECTS("UUAF")); // 4 ECTS
	println!("{}", print_ECTS("EP"));   // Nema unesenog predmeta
}

