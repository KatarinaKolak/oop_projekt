mod first_mod {
	pub fn first_fn(param: String){
		print!("Your input: {} \n", param);
	}
}

// definicija pomocu use 
use first_mod::{
	first_fn
};

// definiranje pomocu as 
use first_mod::first_fn as fun;

fn main() {
	first_mod::first_fn(String::from("Something!"));  // poziv bez definiranja use 
	first_fn(String::from("Something!")); // ne trebamo pozivati college::college_fn jer postoji use 
	fun(String::from("Something!"));  // zbog definiranja as 
} 
