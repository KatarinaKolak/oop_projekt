
// funkcija bez parametara 
fn prva_funcija(){
    println!("Ovo je funkcija bez parametara!");
}

// funkcija sa parametrima
fn druga_funkcija ( x: i32, y: i32){
    let zbroj = x + y;
    println!("Zbroj parametara je: {}", zbroj);
}


// funkcija sa povratnom vrijednosti 
fn povratna(x: i32, y: i32) -> i32{ // iza strelice je tip povratne vrijednosti
    let zbroj = x + y;
	zbroj  // oznacava povratnu vrijednost pa nema tocke zareza na kraju
}

fn main(){
    println!("Povratna vrijednosti je: {}", povratna(5,6)); // Povratna vrijednost je: 11
}

