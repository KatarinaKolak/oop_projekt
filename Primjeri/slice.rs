fn main(){
	let arr = [1, 2, 3, 4, 5];   
	let all_arr = &arr[..]; 
	let slice_arr = &arr[2..4]; 
	
	for i in arr.iter(){
         println!("Element je: {}", i);   //  1, 2, 3, 4, 5
    }
	println!("**********************************************************");
	
	 for i in all_arr.iter(){  // 1, 2, 3, 4, 5
         println!("Element je: {}", i);
    }
	println!("**********************************************************");
	
	for i in slice_arr.iter(){  // 3, 4
         println!("Element je: {}", i);
    }
	println!("**********************************************************");
	
}

