fn printMessageInt(a: i32, b: i32) {   // funkcija koja prima int
	println!("Int implementation!");
}

fn printMessageFloat(a: f32, b: f32) {   // funkcija koja prima float
	println!("Float implementation!");
}

fn main(){
	printMessageInt(2, 3);  // poziv sa intengerima
	printMessageFloat(2.1, 3.2);   // poziv sa floatom
}


