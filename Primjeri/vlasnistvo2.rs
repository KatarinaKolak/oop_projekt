fn main(){
	let mut string = String::from("MyString");  // stvara se objekt na heapu
	add(string);             // string se premjesta u funkciju add
	println!("In main {}",string);    // Greska!!! Ne mozemo isprintati string ovdje ( vlasnistvo je preneseno )
}
fn add(mut string: String){
	string.push_str("AddToEnd");
	println!("{}",string);  // MyStringAddToEnd
}