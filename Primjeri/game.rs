trait Game {
    fn play(&self, num:i32);
	fn set_num(&self)->i32;
}

struct Player {
    name: String,
}

impl Player {
    fn new(name: String) -> Player { 
        Player { name: name }
    }
	pub fn get_name(&self){
        println!("Player name: {}", self.name);
    }
}

impl Game for Player{
	fn set_num(&self)->i32{
		let mut line = String::new();
		println!("Enter number:");
		std::io::stdin().read_line(&mut line); 
		let i: i32 =  line.trim().parse().expect("Error!");  // string -> intenger
		i
	}
	
    fn play(&self, num:i32){
		let mut f = false;
	  
		while f != true{
			let mut line = String::new();
			println!("Guess number:");
			std::io::stdin().read_line(&mut line); 
			let i: i32 =  line.trim().parse().expect("Error!");
		
			if i < num{
				println!("Try a larger number!");
				f = false;
			}
			else if i > num{
				println!("Try a smaller number!");
				f = false;
			}
			else{
				f = true;
				println! ("Win! Number: {} ", num);
			}
		}
	} 
}

 fn main() {
    let mut name1 = String::new();
    println!("Enter name:");
    std::io::stdin().read_line(&mut name1).unwrap(); 
    let player1: Player = Player::new(name1);
	let num = player1.set_num();
	
	println!("Enter name:");
	let mut name2 = String::new();
    std::io::stdin().read_line(&mut name2).unwrap(); 
    let player2 = Player::new(name2);
    player2.play(num);
}

 