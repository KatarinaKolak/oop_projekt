mod college {
   
    pub struct student {
        pub name: String,
		pub course: String, 
    }

    pub struct employee {
         name: String,
		 position: String,
    }

    impl student {
        pub fn print(&self){
            print!("Student name: {} course: {} ", self.name, self.course);
        }
    }
}

fn main() {
    let student1 = college::student { name: String::from("Student1") , course: String::from("IT")};
    student1.print();

    let employee1 = college::employee {name: String::from("Employee1"), position: String::from("Professor") };  // greska
    // liniju nije moguce ispisati jer su clanovi definirani kao privatni i ne mozemo im pristupiti izvan mod bloka
    // kako bi im mogli pristupiti mozemo implementirati konstruktor ili clanove proglasiti javnima
}
