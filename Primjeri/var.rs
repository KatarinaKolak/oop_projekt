fn main(){
	let nepromjenjiva = 105;
	println!("{}", nepromjenjiva);  // 105
	nepromjenjiva = 100; // Greska! nije definirana kao promjenjiva
	let mut promjenjiva = 107;  // mut oznacava da se moze mijenjati
	println!("{}", promjenjiva);  // 107
	promjenjiva = 100;
	println!("{}", promjenjiva);  // 100
}

