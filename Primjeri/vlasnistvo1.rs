fn main() {
	let string1 = "string1value";   // string literal
    let string2 = string1;   // kopira se vrijednost literala string1 i pridodaje se vlasniku string2

    println!("{}", string1);    // string1value
	println!("{}", string2);   // string1value
	
	let string = String::from("stringvalue");    
	let string_2 = string;   // vrijednost string se premjesta u string string_2
	
	println!("{}", string);  // Greska! Jer je vlasnistvo preneseno na string string_2
	println!("{}", string_2);  // stringvalue
}