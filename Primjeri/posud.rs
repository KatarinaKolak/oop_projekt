fn main(){
	let mut string:String = String::from("String len:");  // stvara se objekt na heapu
	print(&mut string); // prosljeduje se referenca funkciji
	println!("Returned value: {}",string);    // ispis -> String len: Odd
}

fn print(ref_string: &mut String) {
	if ref_string.len() % 2 == 0{
		ref_string.push_str(" Even" );
	}
	else{
		ref_string.push_str(" Odd");
	}
}


let y = Box::new(x);