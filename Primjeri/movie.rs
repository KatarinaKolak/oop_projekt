struct Movie {  
   name:String,
   director:String,
   publish_year:u32
}

impl Movie{  // implementacija metode struktre Movie
	fn set_movie(name: String, director: String, publish_year: u32)-> Movie{ // kreiranje filma - konstruktor sa parametrima
		Movie { name: name, director: director, publish_year: publish_year }
	}
}

impl Movie{
	fn print(&self){   // ispis filma ( self je trenutni objekt)
		println!("Director: {} movie: {} publish year: {}", self.director, self.name, self.publish_year);
	}
}

impl Movie{
	fn set_name(&mut self, name: String)-> &mut Movie{   // postavljanje imena filma, vraca se objekt ( mut self jer se objekt mijenja)
		self.name = name;
		self
	}
}

fn main() {
   let movie = Movie::set_movie(String::from("Somewhere"), String::from("Sofia Coppola"), 2010);
   movie.print();  // Director: Sofia Coppola movie: Somewhere publish_year: 2010
   let mut new_movie = Movie:: set_movie(String::from("Lost"), String::from("Sofia Coppola"), 2003);
   new_movie.set_name(String::from("Lost in Translation"));
   new_movie.print(); // Director: Sofia Coppola movie: Lost in Translation publish_year: 2003
}

