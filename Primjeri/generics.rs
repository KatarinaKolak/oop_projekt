struct SGen<T>(T);   // definiranje generickog tipa T

fn printMessage<T>(a: SGen<T>, b: SGen<T>) {   // mozemo poslati i float i intenger
	println!("Function sum!");
}

fn main(){
	printMessage(SGen(2), SGen(3));  // poziv sa intengerima
	printMessage(SGen(2.1), SGen(3.2));   // poziv sa floatom
}



