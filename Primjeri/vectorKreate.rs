fn main(){ 

	let v: Vec<i32> = Vec::new();   // kreiranje vektora pomocu new
	
	let ve: Vec<i32> = vec![];  // kreiranje vektora makronaredbom vec!
	
	let vec = vec![1, 2, 3, 4, 5]; // 1, 2, 3, 4, 5
	
	let vec1 = vec![1, 2, 3, 4, 5];  // 1, 2, 3, 4, 5
	
	let vec2 = vec![1; 5]; // 1, 1, 1, 1, 1
	
}