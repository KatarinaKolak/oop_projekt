struct Movie {
   name:String,
   director:String,
   publish_year:u32
}

// deklaracija - kljucna rijec trait
trait Print {
   fn print(&self);
}

// implementacija - kljucna rijec impl
impl Print for Movie {
   fn print(&self){
            println!("Name:{}  Director: {} Year: {}",self.name,self.director, self.publish_year)
   }
}

fn main() {
   let movie1 = Movie {
      name:String::from("Somewhere"),
      director:String::from("Coppola"),
      publish_year:2010
   };  
   movie1.print()  // poziv funkcije print definirane pomocu trait-a
}




