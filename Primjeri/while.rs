fn main(){
	let mut i = 0;
    while i < 10{  //uvjet-> izvrsava se dok je istinit
		i += 1;
		if i % 2 == 0{
			println!("Broj {} je paran!\n", i);
		}
		else{
			println!("Broj {} je neparan!\n", i);
		}
	}
}


fn main(){
    for i in 1..11{  
		if i % 2 == 0{
			println!("Broj {} je paran!\n", i);
		}
		else{
			println!("Broj {} je neparan!\n", i);
		}
	}
}
