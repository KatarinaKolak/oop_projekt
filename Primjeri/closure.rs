fn main() {
	let mut increment = | param | {  // closure funkcija
		param + 1  // uveca parametar koji primi
	};
	let a = 10;
	print!("{} ", a);  // 10
	print!("{} ", increment(a));  // 11
}


