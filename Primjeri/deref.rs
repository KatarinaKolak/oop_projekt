use std::ops::Deref;  

struct Person {
    name: String,
    year: u32
}

impl Deref for Person {
    type Target = String;   // drugi način deklariranja generičkog tipa

    fn deref(&self) -> &String {  
        &self.name    // posudba selfa onog podatka kojem zelimo pristupiti
    }
}

fn main() {
    let first_person = Person {  // deref metoda kako bi mogli vratiti odredeni tip podataka
        name: String::from("Person1"),
        year: 22,
    };

	println!("{} ", *first_person);  // Person1
}


