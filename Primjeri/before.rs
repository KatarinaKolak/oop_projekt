use std::thread;

fn main() {
	let handle = thread::spawn(|| {   // definiranje nove niti 
       	let mut v = vec![1,2,3];
		v.push(4);
		v.push(5);
		for i in v.iter(){
			println!(" {} ", i);
		}
    });
    // glavna nit
	let mut ve = vec![10,11];  
	ve.push(12);
	ve.push(13);
   for i in ve.iter(){    
		print!("{} ", i);  
   }
   print! ("\n");
   handle.join().unwrap();  // join poziv nakon glavne niti
}

