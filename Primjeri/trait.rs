use std::rand;
use std::rand::Rng;

struct Player {
    name: String
}
 
trait Game {
    fn play(&self);
}
 
impl Player {
    fn new(name: String) -> Player { 
        Movie { name: name }
    }
    fn set_name(&self, name: String){
        self.name = name;
    }
    fn get_name(&self) -> String{
        self.name
    }
	fn print(&self){
        println!("Player name: {} ", self.name);
    }
}
 
impl Game for Player{
     fn play(&self, num:i32){
        let mut line = String::new();
        println!("Enter number:");
        std::io::stdin().read_line(&mut line).unwrap(); 
        while line != num{
            println!("Enter name:");
            std::io::stdin().read_line(&mut line).unwrap();
            if line < num{
                println!("Try a larger number!");
            }
            else{
                println!("Try a smaller number!");
            }
        }
     }
     println! ("Number: {} ", num);
}

 fn main() {
    let mut line = String::new();
    println!("Enter name:");
    std::io::stdin().read_line(&mut line).unwrap(); 
    let player::Player = Player::new(line);
    let num = rand::thread_rng().gen_range(0, 100);
    //println!("Player name: {} \n", player.get_name());
    player.play(num);
 }
 