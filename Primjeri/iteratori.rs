fn main() {
	// princip vlasnistva
   let v = vec![1,2,3,4,5];      
   for mut i in v.into_iter(){  // ovaj iterator premjesta vrijednost na objekt iteratora
	   i = i + 1;  // zbog premjestanja mozemo mijenjati varijablu i
       print!("{} ", i);  // 2, 3, 4, 5, 6
   }
   
   // princip posudivanja
   let arr = [1, 2, 3, 4, 5];
   for i in arr.iter(){  // vraca referencu na svaki element kolekcije
	   print! ("{} ", i);
   }
   
   // iteracija kroz string pomocu chars() metode
   let string = String::from("MyString");
   for i in string.chars(){  // za iteraciju kroz string koristimo chars()
	   print! ("{} ", i);
   }
}


