struct Person {
    name: String,
    year: u32
}

impl Drop for Person {  // pozove se kad varijabla ilazi iz opsega
    fn drop(&mut self) {
		self.year = 23;   // povecamo godine
        println!("Year value: {}", self.year);  // Year value: 23
    }
}

fn main() {
	let p1 = Person{ 
		name: String::from("Person1"),
        year: 22,};  
}